﻿using uMod.IO;

namespace uMod.Game.SCPSecretLaboratory
{
    public class SCPSecretLaboratoryConfiguration : TomlFile
    {
        /// <summary>
        /// Create a new instance of a server configuration
        /// </summary>
        /// <param name="filename"></param>
        public SCPSecretLaboratoryConfiguration(string filename) : base(filename)
        {
        }
    }
}
